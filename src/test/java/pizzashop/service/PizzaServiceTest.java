package pizzashop.service;

import org.junit.jupiter.api.*;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import static org.junit.jupiter.api.Assertions.*;

class PizzaServiceTest {

    private MenuRepository menuR;
    private PaymentRepository pR;
    private PizzaService pizzaService;

    @BeforeEach
    void init(){
        menuR= new MenuRepository();
        pR=new PaymentRepository();
        pizzaService=new PizzaService(menuR,pR);
    }
    @Test
    @DisplayName("Total Valid Card")
    void totalValidCard(){

        //Act
        double nr=pizzaService.getTotalAmount(PaymentType.Card);

        //Assert
        assert nr==243.25;
    }

    @Test
    @DisplayName("Total Valid Cash")

    void totalValidCash(){

        //Act
        double nr=pizzaService.getTotalAmount(PaymentType.Cash);

        //Assert
        assert nr==264.87;
    }


    @Test
    @DisplayName("Total Invalid Input-Numerar")
    void totalInvalidInput(){

        //Act
        double nr=pizzaService.getTotalAmount(PaymentType.Numerar);

        //Assert
        assert nr==0;
    }

    @Test
    @DisplayName("Total Valid Input- invalid")
    void totalValidInput(){

        //Act
        double nr=pizzaService.getTotalAmount(null);

        //Assert
        assert nr==0;
    }

}