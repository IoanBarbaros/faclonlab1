package pizzashop.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;


class PizzaShopTest {

    @Test
    @Disabled
    void testgetpayments(){

    }

    @Test
    @DisplayName("EPC Valid")
    @Order(1)
    void addValidPlata(){

        //Arrange
        MenuRepository menuR= new MenuRepository();
        PaymentRepository pR=new PaymentRepository();
        PizzaService pizzaService=new PizzaService(menuR,pR);

        //Act
        int nr=pizzaService.getPayments().size();
        pizzaService.addPayment(5, PaymentType.Card,25.5);
        int nr1=pizzaService.getPayments().size();
        pizzaService.addPayment(7, PaymentType.Card,75.5);


        //Assert
        assert nr1==nr+1;
        assert pizzaService.getPayments().size()==nr1+1;
    }

    @Test
    @DisplayName("EPC NonValid")
    @Order(2)
    void addNonValidPlata(){
        //Arrange
        MenuRepository menuR= new MenuRepository();
        PaymentRepository pR=new PaymentRepository();
        PizzaService pizzaService=new PizzaService(menuR,pR);

        //Act
        int nr=pizzaService.getPayments().size();
        pizzaService.addPayment(-2, PaymentType.Card,25.5);
        int nr1=pizzaService.getPayments().size();
        pizzaService.addPayment(10, PaymentType.Card,25.5);
        int nr2=pizzaService.getPayments().size();
        pizzaService.addPayment(4 , PaymentType.Card,-25.5);
        int nr3=pizzaService.getPayments().size();
        pizzaService.addPayment(0, PaymentType.Cash,54.5);

        //Assert negative table
        assert pizzaService.getPayments().size()==nr;

        //Assert table>8
        assert pizzaService.getPayments().size()==nr1;

        //Assert negative amount
        assert pizzaService.getPayments().size()==nr2;

        //Assert non valid table
        assert pizzaService.getPayments().size()==nr3;
    }
    @Test
    @DisplayName("BVA Valid")
    @Order(3)
    void addValidPlataBVA(){

        //Arrange
        MenuRepository menuR= new MenuRepository();
        PaymentRepository pR=new PaymentRepository();
        PizzaService pizzaService=new PizzaService(menuR,pR);

        //Act
        int nr=pizzaService.getPayments().size();
        pizzaService.addPayment(1, PaymentType.Card,25.5);
        int nr1=pizzaService.getPayments().size();
        pizzaService.addPayment(8, PaymentType.Card,45.5);
        int nr2=pizzaService.getPayments().size();
        pizzaService.addPayment(2, PaymentType.Card,25.5);
        int nr3=pizzaService.getPayments().size();
        pizzaService.addPayment(7, PaymentType.Card,25.5);
        int nr4=pizzaService.getPayments().size();

        pizzaService.addPayment(7, PaymentType.Card,1);
        int nr5=pizzaService.getPayments().size();
        pizzaService.addPayment(7, PaymentType.Card,2000);
        int nr6=pizzaService.getPayments().size();
        pizzaService.addPayment(7, PaymentType.Card,1999);
        int nr7=pizzaService.getPayments().size();
        pizzaService.addPayment(7, PaymentType.Card,2);

        //Assert for table
        assert nr1==nr+1;
        assert nr2==nr1+1;
        assert nr3==nr2+1;
        assert nr4==nr3+1;

        //Asert for amount
        //Amount=1
        assert nr5==nr4+1;
        //Amount=2000
        assert nr6==nr5+1;
        //Amount=1999
        assert nr7==nr6+1;
        //Amount=2
        assert pizzaService.getPayments().size()==nr7+1;


    }
    @Test
    @DisplayName("BVA Nonvalid")
    @Order(4)
    void addNonvalidPlataBVA(){

        //Arrange
        MenuRepository menuR= new MenuRepository();
        PaymentRepository pR=new PaymentRepository();
        PizzaService pizzaService=new PizzaService(menuR,pR);

        //Act
        int nr=pizzaService.getPayments().size();
        pizzaService.addPayment(0, PaymentType.Card,25.5);
        int nr1=pizzaService.getPayments().size();
        pizzaService.addPayment(9, PaymentType.Card,45.5);
        int nr2=pizzaService.getPayments().size();
        pizzaService.addPayment(2, PaymentType.Card,0);
        int nr3=pizzaService.getPayments().size();
        pizzaService.addPayment(2, PaymentType.Card,2001);

        //Assert table=0
        assert pizzaService.getPayments().size()==nr;

        //Assert table=9
        assert pizzaService.getPayments().size()==nr1;

        //Assert amount=0
        assert pizzaService.getPayments().size()==nr2;
        //Assert amount=2001
        assert pizzaService.getPayments().size()==nr3;

    }

}